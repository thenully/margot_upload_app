
- Docker image build

docker build -t  192.168.0.9:5000/thenully/margot_upload_app:0.2 .

- Docker publish

docker push 192.168.0.9:5000/thenully/margot_upload_app:0.2 

helios create margot_upload_app_ssl:0.2 192.168.0.9:5000/thenully/margot_upload_app:0.2 -p http=80:8003 -p https=443:8004 --volume /data1:/var/data

