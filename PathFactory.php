<?php

/**
 * Created by PhpStorm.
 * User: scott
 * Date: 08/12/2016
 * Time: 7:37 PM
 */
class PathFactory {
    private $mount = "/var/data";
    private $sessionKey;
    private $relPath;
    private $pathName;
    private $ext;

    public $supported = ["jpg", "png", "jpeg", "tif"];
    public $disallowed = array('exe', 'com');

    public function __construct($sessionKey, $ext) {
        $this->sessionKey = $sessionKey;
        $this->ext = $ext;
    }

    function makePath() {
        $pathComponents = str_split($this->sessionKey, 4);
        $last = array_pop($pathComponents);
        $this->relPath = implode( "/", $pathComponents);
        $this->pathName = $last . '.' . $this->ext;
        $path = $this->mount . '/' . $this->relPath;
        if (mkdir($path, 0777, true)) {
            return $path . '/' . $this->pathName;
        }
        return NULL;
    }

    function relPathName() {
        return $this->relPath . '/' . $this->pathName;
    }

    function downPath($path) {
        $fullPath = $this->mount . '/' . $path;
        if (file_exists($fullPath)) {
            $this->ext = pathinfo($fullPath, PATHINFO_EXTENSION);
            return $fullPath;
        }
        return NULL;
    }

    function extname() {
        return $this->ext;
    }

    function mimeType($path) {
        return mime_content_type($path);
    }
}
