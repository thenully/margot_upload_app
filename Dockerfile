FROM php:7.1-apache

#Dockerfile 을 생성/관리하는 사람
MAINTAINER Developer Thenully <dev@thenully.com>

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y libmagickwand-dev imagemagick --no-install-recommends \
  && pecl install imagick \
  && docker-php-ext-enable imagick \
  && rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite ssl headers

RUN mkdir -p /var/data
RUN chmod 0777 /var/data
VOLUME /var/data


COPY config/upload.ini /usr/local/etc/php/conf.d/uploads.ini
COPY config/000-default.conf /etc/apache2/sites-enabled/000-default.conf

COPY config/STAR.reworkapp.com.key /etc/apache2/site-enabled/STAR.reworkapp.com.key
COPY config/STAR.reworkapp.com.crt /etc/apache2/site-enabled/STAR.reworkapp.com.crt

COPY PathFactory.php /var/www/html/PathFactory.php
COPY upload.php /var/www/html/upload.php
COPY download.php /var/www/html/download.php

EXPOSE 8003
EXPOSE 8004