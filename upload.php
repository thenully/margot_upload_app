<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 08/12/2016
 * Time: 3:04 PM
 */

require_once("PathFactory.php");

function generateRandomString($length = 20) {
    $bytes = openssl_random_pseudo_bytes( $length / 2, $cstrong);
    $randomString = bin2hex($bytes);
    return $randomString;
}


if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
    $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

    $session = generateRandomString(20);
    $pathFactory = new PathFactory($session, $extension);

    if(in_array(strtolower($extension), $pathFactory->disallowed)){
        echo '{"status":3}';
        exit;
    }


    $fullPath = $pathFactory->makePath();
    $movePath = $pathFactory->relPathName();
    if ( $fullPath != NULL && $movePath != NULL ) {
        if(move_uploaded_file($_FILES['upl']['tmp_name'], $fullPath)) {
            if(in_array(strtolower($extension), $pathFactory->supported)) {
                $image = new Imagick($fullPath);
                $d = $image->getImageGeometry();
                echo json_encode( array("status" => 0, "url" => $movePath, "dimension" => $d) );
                exit;
            }

            echo json_encode( array("status" => 0, "url" => $movePath) );
            exit;
        }
    }
    echo '{"status":1}';
} else {
    echo '{"status":2}';
}
