<?php
/**
 * Created by PhpStorm.
 * User: scott
 * Date: 08/12/2016
 * Time: 6:44 PM
 */

if(!isset($_SERVER['QUERY_STRING'])){
    header("HTTP/1.0 404 Not Found");
    exit;
}

require_once("PathFactory.php");

$query_str=$_SERVER['QUERY_STRING'];
parse_str($query_str, $query_params);

$pathFactory = new PathFactory(NULL, NULL);

$path = $query_params["url"];

$downPath = $pathFactory->downPath($path);

$fileName = $query_params["name"];

if (($downPath != NULL) && (is_file($downPath = realpath($downPath)) === true)) {
    $baseDirectory = pathinfo($downPath, PATHINFO_DIRNAME);
    $baseFileName = pathinfo($downPath, PATHINFO_FILENAME);
    $baseExtName = pathinfo($downPath, PATHINFO_EXTENSION);

    if (!(empty($query_params["size"]) === true)) {
        preg_match("/([0-9]+)x([0-9]+)/", $query_params["size"], $desiredResolutions);
        if(in_array(strtolower($baseExtName), $pathFactory->supported) && count($desiredResolutions) == 3) {
            $desiredResolutions = array_splice($desiredResolutions, 1, 2);
            $newResolutionPath = $baseDirectory . "/" . $baseFileName . "_" . implode("x", $desiredResolutions) . '.' . $baseExtName;

            if (is_file($newResolutionPath) === true) {
                Download($newResolutionPath, $fileName);
                exit;
            } else {
                $image = new Imagick($downPath);
                autoRotateImage($image);


                $geo = $image->getImageGeometry();
                $width = $desiredResolutions[0];
                $height = $desiredResolutions[1];
                // crop the image
                if(($geo['width']/$width) < ($geo['height']/$height))
                {
                    $image->cropImage($geo['width'], floor($height*$geo['width']/$width), 0, (($geo['height']-($height*$geo['width']/$width))/2));
                }
                else
                {
                    $image->cropImage(ceil($width*$geo['height']/$height), $geo['height'], (($geo['width']-($width*$geo['height']/$height))/2), 0);
                }
                $image->thumbnailImage($width, $height, true);
                $image->writeImage($newResolutionPath);
                if (is_file($newResolutionPath) === true) {
                    Download($newResolutionPath, $fileName);
                    exit;
                }
            }
        }
    }

    Download($downPath, $fileName);
    exit;
}

header(sprintf('%s %03u %s', 'HTTP/1.1', 404, 'Not Found'), true, 404);
exit;


// Note: $image is an Imagick object, not a filename! See example use below.
function autoRotateImage($image) {
    $orientation = $image->getImageOrientation();

    switch($orientation) {
        case imagick::ORIENTATION_BOTTOMRIGHT:
            $image->rotateimage("#000", 180); // rotate 180 degrees
            $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
            break;

        case imagick::ORIENTATION_RIGHTTOP:
            $image->rotateimage("#000", 90); // rotate 90 degrees CW
            $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
            break;

        case imagick::ORIENTATION_LEFTBOTTOM:
            $image->rotateimage("#000", -90); // rotate 90 degrees CCW
            $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
            break;
    }

    // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
}


function Download($path, $fileName = null, $speed = null, $multipart = true)
{
    while (ob_get_level() > 0)
    {
        ob_end_clean();
    }

    if (is_file($path = realpath($path)) === true)
    {
        $file = @fopen($path, 'rb');
        $size = sprintf('%u', filesize($path));
        $lastModified = date('D, d M Y H:i:s T', filemtime($path));
        $speed = (empty($speed) === true) ? 1024 : floatval($speed);

        if (is_resource($file) === true)
        {
            set_time_limit(0);

            if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
                if($lastModified == $_SERVER['HTTP_IF_MODIFIED_SINCE']) {
                    header('HTTP/1.1 304 Not Modified');
                    header('Cache-Control: private, must-revalidate, max-age=0');
                    exit;
                }
            }

            if ($multipart === true)
            {
                $range = array(0, $size - 1);

                if (array_key_exists('HTTP_RANGE', $_SERVER) === true)
                {
                    $range = array_map('intval', explode('-', preg_replace('~.*=([^,]*).*~', '$1', $_SERVER['HTTP_RANGE'])));

                    if (empty($range[1]) === true)
                    {
                        $range[1] = $size - 1;
                    }

                    foreach ($range as $key => $value)
                    {
                        $range[$key] = max(0, min($value, $size - 1));
                    }

                    if (($range[0] > 0) || ($range[1] < ($size - 1)))
                    {
                        header(sprintf('%s %03u %s', 'HTTP/1.1', 206, 'Partial Content'), true, 206);
                    }
                }

                header('Accept-Ranges: bytes');
                header('Content-Range: bytes ' . sprintf('%u-%u/%u', $range[0], $range[1], $size));
            }

            else
            {
                $range = array(0, $size - 1);
            }

            $content_type = mime_content_type($path);

            header('Content-Type: ' . $content_type);
            header('Content-Length: ' . sprintf('%u', $range[1] - $range[0] + 1));

            if (empty($fileName) !== true) {
                header('Content-Disposition: attachment; filename="' . addslashes($fileName) . '"');
            }

            header('Content-Transfer-Encoding: binary');

            if ($range[0] > 0)
            {
                fseek($file, $range[0]);
            }

            header('Last-Modified: '.$lastModified);

            while ((feof($file) !== true) && (connection_status() === CONNECTION_NORMAL))
            {
                echo fread($file, round($speed * 1024)); flush(); sleep(1);
            }

            fclose($file);
        }

        exit();
    }

    else
    {
        header(sprintf('%s %03u %s', 'HTTP/1.1', 404, 'Not Found'), true, 404);
    }

    return false;
}
